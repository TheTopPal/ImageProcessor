#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QtConcurrent/QtConcurrent>
#include <QMessageBox>

#include "Core/algorithmsettings.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),
    m_algorithmManager(AlgorithmManager::getInstance())
{
    ui->setupUi(this);

    this->ui->comboBoxAlgorithm->setModel(m_algorithmManager.getComboBoxModel());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_menuFileOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(
        this,
        "Выбрать изображение",
        "", // Стандартная директория
        "Изображения (*.bmp *.jpg *.png)");

    QImage sourceImage(fileName);

    if(sourceImage.isNull()) {
        QMessageBox::warning(this, "Внимание", "Пожалуйста, выберите файл изображения, чтобы продолжить работу с программой.");

        return;
    }

    int algorithmIndex = ui->comboBoxAlgorithm->currentIndex();

    emit ui->comboBoxAlgorithm->activated(algorithmIndex);

    m_algorithmManager.setSourceImage(sourceImage);

    ui->labelImage->setPixmap(QPixmap::fromImage(sourceImage));
    ui->labelImage->setScaledContents(true);
}


void MainWindow::on_menuFileSave_triggered()
{
    if(m_algorithmManager.getSourceImage().isNull()) {
        QMessageBox::warning(this, "Внимание", "Перед сохранением, пожалуйста, откройте файл с изображением в программе.");
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    "Выбрать изображение",
                                                    "",
                                                    "Изображения (*.bmp *.jpg *.png)");

    if(!fileName.isEmpty()) {
        ui->labelImage->pixmap(Qt::ReturnByValue).save(fileName);
        QMessageBox::information(this, "Уведомление", "Файл с именем " + QString(fileName) + " успешно сохранён.");
    } else {
        QMessageBox::warning(this, "Внимание", "Вы не указали имя файла. Сохранение отменено.");
    }
}


void MainWindow::on_menuFileRestoreSource_triggered()
{
    QImage loadedImage(m_algorithmManager.getSourceImage());

    if (loadedImage.isNull()) {
        QMessageBox::warning(this, "Внимание", "Файл не был загружен. Восстановление исходного изображение невозможно.");
    }
    else {
        ui->labelImage->setPixmap(QPixmap::fromImage(loadedImage));
    }

}


void MainWindow::on_menuFileQuit_triggered()
{
    QApplication::quit();
}


void MainWindow::on_pushButtonGrayscale_clicked()
{
    if (m_algorithmManager.getSourceImage().isNull()) {
        QMessageBox::warning(this, "Внимание", "Файл не был загружен. Перед работой необходимо загрузить изображение.");

        return;
    }

    QFuture<QImage> future = QtConcurrent::run([=]() -> QImage {
        return m_algorithmManager.runAlgorithm(1);
    });

    future.then([=](const QImage& processedImage) {
        QPixmap pixmap = QPixmap::fromImage(processedImage);
        ui->labelImage->setPixmap(pixmap);
    });
}

void MainWindow::on_horizontalSliderContrast_valueChanged(int value)
{
    if (m_algorithmManager.getSourceImage().isNull()) {
        QMessageBox::warning(this, "Внимание", "Файл не был загружен. Перед работой необходимо загрузить изображение.");
        return;
    }


    QSharedPointer<Core::AlgorithmSettings> settings = QSharedPointer<Core::AlgorithmSettings>::create();
    settings->setImage(m_algorithmManager.getSourceImage());
    settings->setSaturation(value);

    QFuture<QImage> future = QtConcurrent::run([=]() -> QImage {
        return m_algorithmManager.runAlgorithmWithCustomSettings(0, settings);
    });

    future.then([=](const QImage& processedImage) {
        QPixmap pixmap = QPixmap::fromImage(processedImage);
        ui->labelImage->setPixmap(pixmap);
    });
}

void MainWindow::on_comboBoxAlgorithm_currentIndexChanged(int index)
{    
    switch(index)
    {
        // Порядок регистрации алгоритмов, у которых имя НЕ скрыто.
    case 0:
        ui->groupBoxContrastSettings->setVisible(true);
        ui->groupBoxGradientSettings->setVisible(false);
        break;

    case 1:
        ui->groupBoxContrastSettings->setVisible(false);
        ui->groupBoxGradientSettings->setVisible(true);
        break;
    }

}


void MainWindow::on_radioButtonSobel_clicked()
{
    if (m_algorithmManager.getSourceImage().isNull()) {
        QMessageBox::warning(this, "Внимание", "Файл не был загружен. Перед работой необходимо загрузить изображение.");

        return;
    }

    QFuture<QImage> future = QtConcurrent::run([=]() -> QImage {
        return m_algorithmManager.runAlgorithm(2);
    });

    future.then([=](const QImage& processedImage) {
        QPixmap pixmap = QPixmap::fromImage(processedImage);
        ui->labelImage->setPixmap(pixmap);
    });
}


void MainWindow::on_radioButtonPrewitt_clicked()
{
    if (m_algorithmManager.getSourceImage().isNull()) {
        QMessageBox::warning(this, "Внимание", "Файл не был загружен. Перед работой необходимо загрузить изображение.");

        return;
    }

    QFuture<QImage> future = QtConcurrent::run([=]() -> QImage {
        return m_algorithmManager.runAlgorithm(3);
    });

    future.then([=](const QImage& processedImage) {
        QPixmap pixmap = QPixmap::fromImage(processedImage);
        ui->labelImage->setPixmap(pixmap);
    });

}


void MainWindow::on_radioButtonScharr_clicked()
{
    if (m_algorithmManager.getSourceImage().isNull()) {
        QMessageBox::warning(this, "Внимание", "Файл не был загружен. Перед работой необходимо загрузить изображение.");

        return;
    }

    QFuture<QImage> future = QtConcurrent::run([=]() -> QImage {
        return m_algorithmManager.runAlgorithm(4);
    });

    future.then([=](const QImage& processedImage) {
        QPixmap pixmap = QPixmap::fromImage(processedImage);
        ui->labelImage->setPixmap(pixmap);
    });
}

