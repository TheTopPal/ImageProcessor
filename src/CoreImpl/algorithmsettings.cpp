#include "Core/algorithmsettings.h"

#include <QImage>

QImage Core::AlgorithmSettings::getImage() const
{
    return m_image;
}

void Core::AlgorithmSettings::setImage(const QImage& img)
{
    m_image = img;
}


int Core::AlgorithmSettings::getSaturation() const
{
    return m_saturation;
}

void Core::AlgorithmSettings::setSaturation(int saturation)
{
    m_saturation = saturation;
}
