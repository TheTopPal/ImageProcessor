#include "Models/customcomboboxmodel.h"

CustomComboBoxModel::CustomComboBoxModel(QObject *parent)
    : QAbstractListModel(parent),
    m_algorithmNames()
{

}

int CustomComboBoxModel::rowCount(const QModelIndex& index) const
{
    return m_algorithmNames.count();
}

QVariant CustomComboBoxModel::data(const QModelIndex &index, int role) const
{
    QVariant value;

    switch ( role )
    {
        case Qt::DisplayRole: // String
        {
            value = this->m_algorithmNames.at(index.row());
        }
        break;


        default:
            break;
    }

    return value;
}

void CustomComboBoxModel::populate(QList<QString> filterNames)
{
    int idx = this->m_algorithmNames.count();
    this->beginInsertRows(QModelIndex(), 1, idx);
        this->m_algorithmNames = filterNames;
    endInsertRows();
}
