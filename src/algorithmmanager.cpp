#include "algorithmmanager.h"

// For Algorithms
#include "Algorithms/contrastalgorithm.h"
#include "Algorithms/grayscalealgorithm.h"
#include "Algorithms/prewittalgorithm.h"
#include "Algorithms/scharralgorithm.h"
#include "Algorithms/sobelalgorithm.h"
//

using namespace Core::Algorithms;

AlgorithmManager::AlgorithmManager()
    : m_algorithms(),
    m_algorithmsHashTable(),
    m_sourceImage(),
    m_customComboBoxModel(new CustomComboBoxModel())
{
    // В целях оптимизации можно сделать так:
    // m_algorithms.reserve(2); - Если мы знаем сколько алгоритмов регистрируем
    initAlgorithms(); // В момент создания менеджера происходит регистрация
    initModels();
}

AlgorithmManager::~AlgorithmManager()
{

}

AlgorithmManager& AlgorithmManager::getInstance()
{
    static AlgorithmManager instance;
    return instance;
}

void AlgorithmManager::registerAlgorithm(QSharedPointer<Core::IAlgorithm> algorithm, QString algorithmName, bool hideNameInComboBox)
{
    if(!algorithm) {
        throw std::invalid_argument("Invalid algorithm");
    }

    m_algorithms.push_back(algorithm);

    if(!algorithmName.isNull()) {
        if (!hideNameInComboBox) {
            m_algorithmsHashTable.insert(algorithmName, algorithm);
        }
    }
}

QSharedPointer<Core::IAlgorithm> AlgorithmManager::getAlgorithmById(int id)
{
    if ((id < 0) || (m_algorithms.size() - 1 < id)) {
        throw std::out_of_range("Out of range");
    }

    return m_algorithms.at(id);
}

int AlgorithmManager::getRegisteredAlgorithmCount() const
{
    return m_algorithms.size();
}

void AlgorithmManager::setSourceImage(const QImage& img)
{
    if (img.isNull()) {
        throw std::invalid_argument("Invalid source image");
    }

    m_sourceImage = img;
}

QImage AlgorithmManager::getSourceImage() const
{
    return m_sourceImage;
};

QVector<QString> AlgorithmManager::getAlgorithmNames() const
{
    QVector<QString> algorithmNames;

    foreach (QString name, m_algorithmsHashTable.keys()) {
        algorithmNames.push_back(name);
    }

    if (algorithmNames.isEmpty()) {
        throw std::logic_error("Algorithms with registered names not found");
    }

    return algorithmNames;
}

void AlgorithmManager::initAlgorithms()
{
    QSharedPointer<ContrastAlgorithm> contrastAlgorithm = QSharedPointer<ContrastAlgorithm>::create();
    QSharedPointer<GrayscaleAlgorithm> grayscaleAlgorithm = QSharedPointer<GrayscaleAlgorithm>::create();
    QSharedPointer<SobelAlgorithm> sobelAlgorithm = QSharedPointer<SobelAlgorithm>::create();
    QSharedPointer<PrewittAlgoritm> prewittAlgorithm = QSharedPointer<PrewittAlgoritm>::create();
    QSharedPointer<ScharrAlgorithm> scharrAlgorithm = QSharedPointer<ScharrAlgorithm>::create();

    // Регистрация алгоритмов
    registerAlgorithm(contrastAlgorithm, "Contrast", false);
    registerAlgorithm(grayscaleAlgorithm, "Grayscale", true);

    registerAlgorithm(sobelAlgorithm, "Gradient (Sobel Operator)", false);
    registerAlgorithm(prewittAlgorithm, "Gradient (Prewitt Operator)", true);
    registerAlgorithm(scharrAlgorithm, "Gradient (Scharr Operator)", true);
}

void AlgorithmManager::initModels()
{
    m_customComboBoxModel->populate(getAlgorithmNames().toList());
}

QAbstractItemModel* AlgorithmManager::getComboBoxModel()
{
    return m_customComboBoxModel;
}


QImage AlgorithmManager::runAlgorithm(int id)
{
    if (m_sourceImage.isNull()) {
        throw std::invalid_argument("Source image is null.");
    }

    QSharedPointer<Core::IAlgorithm> currentAlgorithm = getAlgorithmById(id);
    QSharedPointer<Core::AlgorithmSettings> algorithmSettings = QSharedPointer<Core::AlgorithmSettings>::create();

    algorithmSettings->setImage(m_sourceImage);

    return currentAlgorithm->applyAlgorithm(algorithmSettings);
}


QImage AlgorithmManager::runAlgorithmWithCustomSettings(int id, QSharedPointer<Core::AlgorithmSettings> settings)
{
    if (m_sourceImage.isNull()) {
        throw std::invalid_argument("Source image is null.");
    }

    QSharedPointer<Core::IAlgorithm> currentAlgorithm = getAlgorithmById(id);
    QSharedPointer<Core::AlgorithmSettings> algorithmSettings = settings;

    algorithmSettings->setImage(m_sourceImage);

    return currentAlgorithm->applyAlgorithm(algorithmSettings);
}
