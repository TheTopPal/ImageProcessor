#include "Algorithms/contrastalgorithm.h"

#include <QImage>
#include <QSharedPointer>
#include <QRgb>

using namespace Core::Algorithms;

QImage ContrastAlgorithm::applyAlgorithm(QSharedPointer<AlgorithmSettings> algorithmSettings)
{
    QImage modifiedImage(algorithmSettings->getImage());
    int saturation = algorithmSettings->getSaturation();

    for (int y = 0; y < modifiedImage.height(); ++y) {

        for (int x = 0; x < modifiedImage.width(); ++x) {

            QRgb rgb = modifiedImage.pixel(x, y);
            int red = qRed(rgb);
            int green = qGreen(rgb);
            int blue = qBlue(rgb);

            red = qMin(255, qMax(0, (int) (red + saturation)));
            green = qMin(255, qMax(0, (int) (green + saturation)));
            blue = qMin(255, qMax(0, (int)(blue + saturation)));

            modifiedImage.setPixelColor(x, y, qRgb(red, green, blue));
        }
    }

    return modifiedImage;

}
