#include "Algorithms/grayscalealgorithm.h"

#include <QImage>
#include <QSharedPointer>
#include <QRgb>

using namespace Core::Algorithms;

QImage GrayscaleAlgorithm::applyAlgorithm(QSharedPointer<AlgorithmSettings> algorithmSettings)
{
    QImage modifiedImage(algorithmSettings->getImage());

    for (int y = 0; y < modifiedImage.height(); ++y) {
        for (int x = 0; x < modifiedImage.width(); ++x) {

            QRgb pixel = modifiedImage.pixel(x, y);
            int grayValue = (qRed(pixel) + qGreen(pixel) + qBlue(pixel)) / 3;

            modifiedImage.setPixel(x, y, qRgb(grayValue, grayValue, grayValue));
        }
    }

    return modifiedImage;
}
