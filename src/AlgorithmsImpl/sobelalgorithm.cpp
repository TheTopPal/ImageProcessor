#include "Algorithms/sobelalgorithm.h"

#include <QImage>
#include <QSharedPointer>
#include <QRgb>

using namespace Core::Algorithms;

QImage SobelAlgorithm::applyAlgorithm(QSharedPointer<AlgorithmSettings> algorithmSettings)
{
    int sobelX[][3] = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
    int sobelY[][3] = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

    QImage modifiedImage = toGrayscale(algorithmSettings);

    int width = modifiedImage.width();
    int height = modifiedImage.height();

    QImage resultImage(width, height, QImage::Format_RGB32);

    for (int y = 1; y < height - 1; ++y) {
        for (int x = 1; x < width - 1; ++x) {
            int pixelX = 0;
            int pixelY = 0;

            for (int i = -1; i <= 1; ++i) {
                for (int j = -1; j <= 1; ++j) {
                    int rgb = qRed(modifiedImage.pixel(x + i, y + j));
                    pixelX += rgb * sobelX[i + 1][j + 1];
                    pixelY += rgb * sobelY[i + 1][j + 1];
                }
            }

            int magnitude = static_cast<int>(sqrt(pixelX * pixelX + pixelY * pixelY));
            int newPixel = (magnitude << 16) | (magnitude << 8) | magnitude;

            resultImage.setPixel(x, y, qRgb(newPixel, newPixel, newPixel));
        }
    }

    return resultImage; // Or Modified Image?

}

QImage SobelAlgorithm::toGrayscale(QSharedPointer<AlgorithmSettings> algorithmSettings)
{
    QImage modifiedImage(algorithmSettings->getImage());

    for (int y = 0; y < modifiedImage.height(); ++y) {
        for (int x = 0; x < modifiedImage.width(); ++x) {

            QRgb pixel = modifiedImage.pixel(x, y);
            int grayValue = (qRed(pixel) + qGreen(pixel) + qBlue(pixel)) / 3;

            modifiedImage.setPixel(x, y, qRgb(grayValue, grayValue, grayValue));
        }
    }

    return modifiedImage;
}
