QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

INCLUDEPATH += $$PWD/include
INCLUDEPATH += $$PWD/src
INCLUDEPATH += $$PWD/forms

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/CoreImpl/algorithmsettings.cpp \
    src/CoreImpl/ialgorithm.cpp \
    src/AlgorithmsImpl/contrastalgorithm.cpp \
    src/AlgorithmsImpl/grayscalealgorithm.cpp \
    src/AlgorithmsImpl/prewittalgorithm.cpp \
    src/AlgorithmsImpl/scharralgorithm.cpp \
    src/AlgorithmsImpl/sobelalgorithm.cpp \
    src/ModelsImpl/customcomboboxmodel.cpp \
    src/algorithmmanager.cpp \
    src/main.cpp \
    src/mainwindow.cpp

HEADERS += \
    include/Core/algorithmsettings.h \
    include/Core/ialgorithm.h \
    include/Algorithms/contrastalgorithm.h \
    include/Algorithms/grayscalealgorithm.h \
    include/Algorithms/prewittalgorithm.h \
    include/Algorithms/scharralgorithm.h \
    include/Algorithms/sobelalgorithm.h \
    include/Models/customcomboboxmodel.h \
    include/algorithmmanager.h \
    include/mainwindow.h

FORMS += \
    forms/mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
