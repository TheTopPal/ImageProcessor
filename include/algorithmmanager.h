#pragma once

#include "Core/ialgorithm.h"
#include "Core/algorithmsettings.h"
#include "Models/customcomboboxmodel.h"

#include <QHash>
#include <QSharedPointer>
#include <QString>
#include <QVector>


class AlgorithmManager final
{
private:
    AlgorithmManager();

    ~AlgorithmManager();

public:
    static AlgorithmManager& getInstance();

    // Algorithms
    void registerAlgorithm(QSharedPointer<Core::IAlgorithm> algorithm, QString algorithmName, bool hideNameInComboBox);
    void initAlgorithms();

    int getRegisteredAlgorithmCount() const;

    QSharedPointer<Core::IAlgorithm> getAlgorithmById(int id);
    QVector<QString> getAlgorithmNames() const;

    QImage runAlgorithm(int id);
    QImage runAlgorithmWithCustomSettings(int id, QSharedPointer<Core::AlgorithmSettings> algorithmSettings);

    // Models
    void initModels();

    QAbstractItemModel* getComboBoxModel();

    // Source Image
    void setSourceImage(const QImage& img);
    QImage getSourceImage() const;

private:
    QVector<QSharedPointer<Core::IAlgorithm>> m_algorithms;
    QHash<QString, QSharedPointer<Core::IAlgorithm>> m_algorithmsHashTable;
    QImage m_sourceImage;

    CustomComboBoxModel* m_customComboBoxModel;

    // NEW
    QSharedPointer<Core::AlgorithmSettings> m_settings;
 };
