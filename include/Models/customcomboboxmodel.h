#pragma once

#include <QModelIndex>

// Namespace

class CustomComboBoxModel : public QAbstractListModel
{
public:
    CustomComboBoxModel(QObject *parent = nullptr);

public:
    int rowCount(const QModelIndex& index) const;
    QVariant data(const QModelIndex &index, int role) const;
    void populate(QList<QString> algorithmNames);

private:
    QList<QString> m_algorithmNames;
};
