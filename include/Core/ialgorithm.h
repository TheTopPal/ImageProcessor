#pragma once

#include "algorithmsettings.h"

#include <QImage>
#include <QSharedPointer>

namespace Core
{
    class IAlgorithm
    {
    public:
        virtual ~IAlgorithm() = default;

    public:
        virtual QImage applyAlgorithm(QSharedPointer<AlgorithmSettings> algorithmSettings) = 0;
    };
}
