#pragma once

#include <QImage>

namespace Core
{
    class AlgorithmSettings final
    {
    public:
        QImage getImage() const;
        void setImage(const QImage& img);

        // For Contrast
        int getSaturation() const;
        void setSaturation(int saturation);

    private:
        QImage m_image; // Source image
        int m_saturation;
    };
}
