#pragma once

#include "algorithmmanager.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_menuFileOpen_triggered();

    void on_menuFileSave_triggered();

    void on_menuFileRestoreSource_triggered();

    void on_menuFileQuit_triggered();

    void on_pushButtonGrayscale_clicked();

    void on_horizontalSliderContrast_valueChanged(int value);

    void on_comboBoxAlgorithm_currentIndexChanged(int index);

    void on_radioButtonSobel_clicked();

    void on_radioButtonPrewitt_clicked();

    void on_radioButtonScharr_clicked();
private:
    Ui::MainWindow *ui;

    AlgorithmManager& m_algorithmManager;
};
