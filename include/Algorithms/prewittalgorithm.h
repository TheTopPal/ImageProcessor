#pragma once

#include "Core/ialgorithm.h"
#include "Core/algorithmsettings.h"

#include <QImage>
#include <QSharedPointer>

namespace Core
{
    namespace Algorithms
    {
        class PrewittAlgoritm : public IAlgorithm
        {
        public:
            QImage applyAlgorithm(QSharedPointer<AlgorithmSettings> algorithmSettings) override;
            QImage toGrayscale(QSharedPointer<AlgorithmSettings> algorithmSettings);
        };
    }
}
