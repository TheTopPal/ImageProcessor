#pragma once

#include "Core/ialgorithm.h"
#include "Core/algorithmsettings.h"

#include <QImage>
#include <QSharedPointer>

namespace Core
{
    namespace Algorithms
    {
        class GrayscaleAlgorithm : public IAlgorithm
        {
        public:
            QImage applyAlgorithm(QSharedPointer<AlgorithmSettings> algorithmSettings) override;
        };
    }
}
